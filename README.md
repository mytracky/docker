## Update

Пересобирет образ, останавливает конейнер, запускает новый контейнер, удаляет старый

```docker-compose up -d --no-deps --build client```

## Common
```apt update```
```apt install git docker docker-compose curl -y```

## Gitlab runner
```curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash```
```apt install gitlab-runner```

```
gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "$TOKEN" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false"
```

## Mysql
```apt update```

```apt install mysql-server```

```mysql```

```CREATE DATABASE mytracky;```

```CREATE USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '$PASSWORD';```

```SELECT User, Host FROM mysql.user;```

```GRANT ALL PRIVILEGES ON mytracky.* TO 'root'@'172.17.0.1';```

```FLUSH PRIVILEGES;```

```nano /etc/mysql/mysql.conf.d/mysqld.cnf```

bind-adress = 0.0.0.0